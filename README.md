## Context

A client can communicate with a git server using 3 different protocols : SSH, HTTP(S) and Git.

Here we will be choosing the SSH protocol, witch is both secure and easy to use. We will need to create a key pair, and take the public key to the remote server.

We can test that the procedure by connecting via SSH.

# Get started

This repository creates 3 Docker containers: one server and two clients. The first one initializes a Git server, while the other two can connect to the server automatically.

## Download the repository

First, you need to create a directory to store the repository with `mkdir`. Then you need to use the command `git clone` to download the repository.
```bash
git clone https://forge.univ-lyon1.fr/p2310768/tp5
```

## Creation of containers

You need to be administrator to create containers : `sudo -i`.
Enter your administrator password.

Use the command `docker compose up` to create the 3 containers.
Verify containers are present : `docker ps -all`.

## How to use Docker ?

Here are some common commands for using Docker:

`docker run` Create and start a new container.
`docker ps` List running containers.
`docker ps -a` List all containers (including stopped ones).
`docker images` List locally available Docker images.
`docker build` Build a Docker image from a Dockerfile.
`docker rm` Remove one or more containers.
`docker stop` Stop one or more running containers.
`docker exec` Run a command in a running container.

It will allow you tu use Docker correctly.
